import { viteBundler } from "@vuepress/bundler-vite";
import { defaultTheme } from "@vuepress/theme-default";
import { defineUserConfig } from "vuepress";
import { registerComponentsPlugin } from "@vuepress/plugin-register-components";

import { getDirname, path } from "@vuepress/utils";

const __dirname = getDirname(import.meta.url);

export default defineUserConfig({
  plugins: [
    registerComponentsPlugin({
      componentsDir: path.resolve(__dirname, "./components"),
    }),
  ],
  locales: {
    "/": {
      lang: "en-US",
      title: "Component Library 🥂",
      description: "Documentation site for the Vue component library plugin",
    },
  },
  bundler: viteBundler({
    viteOptions: {},
    vuePluginOptions: {},
  }),
  theme: defaultTheme({
    repoLabel: "Contribute!",
    // git repo here... gitlab, github
    repo: "",
    docsDir: "docs",
    editLink: true,
    docsBranch: "dev",
    editLinkText: "Help us improve this page!",
    locales: {
      "/": {
        label: "English",
        selectText: "Languages",
        lastUpdated: "Last Updated",
        // service worker is configured but will only register in production
        serviceWorker: {
          updatePopup: {
            message: "New content is available.",
            buttonText: "Refresh",
          },
        },
        navbar: [
          { text: "Getting Started", link: "/guide" },
          { text: "components", link: "/components/" },
          // external link to git repo...again
          { text: "GitHub", link: "" },
        ],
        sidebar: {
          "/components/": [
            {
              title: "components",
              collapsable: false,
              children: ["standard-component"],
            },
          ],
        },
      },
    },
  }),
});
