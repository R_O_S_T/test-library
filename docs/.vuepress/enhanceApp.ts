import ComponentLibrary from "./../../src/main";
import { App } from "vue";

export default ({ Vue }: { Vue: App }) => {
  Vue.use(ComponentLibrary);
};
