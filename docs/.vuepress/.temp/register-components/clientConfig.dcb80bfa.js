import { defineAsyncComponent } from 'vue'

export default {
  enhance: ({ app }) => {    
      app.component("Demo", defineAsyncComponent(() => import("/Users/rost/Projects/vue-component-library/docs/.vuepress/components/Demo.vue"))),
      app.component("SourceCode", defineAsyncComponent(() => import("/Users/rost/Projects/vue-component-library/docs/.vuepress/components/SourceCode.vue"))),
      app.component("v-hello", defineAsyncComponent(() => import("/Users/rost/Projects/vue-component-library/docs/.vuepress/components/v-hello.vue"))),
      app.component("examples-standard-component-doc", defineAsyncComponent(() => import("/Users/rost/Projects/vue-component-library/docs/.vuepress/components/examples/standard-component-doc.vue")))
  },
}
