export const redirects = JSON.parse("{}")

export const routes = Object.fromEntries([
  ["/", { loader: () => import(/* webpackChunkName: "v-4ec95bbd" */"/Users/rost/Projects/vue-component-library/docs/.vuepress/.temp/pages/index.html.js"), meta: {"title":""} }],
  ["/guide.html", { loader: () => import(/* webpackChunkName: "v-262913d3" */"/Users/rost/Projects/vue-component-library/docs/.vuepress/.temp/pages/guide.html.js"), meta: {"title":"Getting Started"} }],
  ["/components/", { loader: () => import(/* webpackChunkName: "v-57cd19aa" */"/Users/rost/Projects/vue-component-library/docs/.vuepress/.temp/pages/components/index.html.js"), meta: {"title":"Components"} }],
  ["/components/standard-component.html", { loader: () => import(/* webpackChunkName: "v-39da7cee" */"/Users/rost/Projects/vue-component-library/docs/.vuepress/.temp/pages/components/standard-component.html.js"), meta: {"title":"standard-component"} }],
  ["/404.html", { loader: () => import(/* webpackChunkName: "v-8fe89ed2" */"/Users/rost/Projects/vue-component-library/docs/.vuepress/.temp/pages/404.html.js"), meta: {"title":""} }],
]);

if (import.meta.webpackHot) {
  import.meta.webpackHot.accept()
  if (__VUE_HMR_RUNTIME__.updateRoutes) {
    __VUE_HMR_RUNTIME__.updateRoutes(routes)
  }
  if (__VUE_HMR_RUNTIME__.updateRedirects) {
    __VUE_HMR_RUNTIME__.updateRedirects(redirects)
  }
}

if (import.meta.hot) {
  import.meta.hot.accept(({ routes, redirects }) => {
    __VUE_HMR_RUNTIME__.updateRoutes(routes)
    __VUE_HMR_RUNTIME__.updateRedirects(redirects)
  })
}
