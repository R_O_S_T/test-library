# standard-component

Wow! This component is awesome.

## Example

<Demo componentName="examples-standard-component-doc" />

## Source Code

<SourceCode>
:[code](../scr/components/Button/Button.vue)
</SourceCode>

## slots

...

## props

...
