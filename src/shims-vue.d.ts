import { App } from "vue";
/* eslint-disable */
declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare global {
  interface Window {
    Vue: App; // 👈️ turn off type checking
  }
}
declare module '@vuepress/plugin-register-components';
