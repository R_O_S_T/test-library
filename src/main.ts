import * as components from "./components";
import { App } from "vue";

const ComponentLibrary = {
  install(Vue: App) {
    // components
    for (const componentName in components) {
      const component = components[componentName as keyof typeof components];

      Vue.component(component.name, component);
    }
  },
};

export default ComponentLibrary;

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(ComponentLibrary);
}
